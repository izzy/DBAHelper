-- =============================================================================
-- Query the Oracle Database alert log via SQL for ORA- errors
--
-- will ask for a time frame (from how many days back to start, and unto how
-- many days to look; e.g. for the last 24h, specify 1 and 0 respectively)
-- =============================================================================

SET linesize 200 pagesize 200
col RECORD_ID for 9999999 head ID
col orig_time for a19 head Occurence
col MESSAGE_TEXT for a120 head Message

SELECT record_id, to_char(originating_timestamp,'yyyy-mm-dd hh24:mi:ss') as orig_time, message_text
FROM X$DBGALERTEXT
WHERE originating_timestamp BETWEEN systimestamp - &days_back_start AND systimestamp - &days_back_end
  AND regexp_like(message_text, '(ORA-|error)')
;
