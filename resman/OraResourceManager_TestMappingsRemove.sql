/* =============================================================================
   Remove additional (temporary) mappings
   -----------------------------------------------------------------------------
   This example shows how you can remove members from a resource group: you
   simply define them again, but set their CONSUMER_GROUP to NULL - effectively
   bumping them down to the OTHERS group.
   ========================================================================== */

begin
  DBMS_RESOURCE_MANAGER.CLEAR_PENDING_AREA();
  DBMS_RESOURCE_MANAGER.CREATE_PENDING_AREA();

  DBMS_RESOURCE_MANAGER.SET_CONSUMER_GROUP_MAPPING(         -- Fake Application Member
    ATTRIBUTE      => DBMS_RESOURCE_MANAGER.ORACLE_USER,
    VALUE          => 'APPMOB%',
    CONSUMER_GROUP => NULL
  );
  DBMS_RESOURCE_MANAGER.SET_CONSUMER_GROUP_MAPPING(         -- Fake BatchGroup Member
    ATTRIBUTE      => DBMS_RESOURCE_MANAGER.ORACLE_USER,
    VALUE          => 'BATCHTEST',
    CONSUMER_GROUP => NULL
  );
  DBMS_RESOURCE_MANAGER.SET_CONSUMER_GROUP_MAPPING(         -- Fake External Member (in case we cannot access the existing ones)
    ATTRIBUTE      => DBMS_RESOURCE_MANAGER.ORACLE_USER,
    VALUE          => 'EXTTEST%',
    CONSUMER_GROUP => NULL
  );

  DBMS_RESOURCE_MANAGER.VALIDATE_PENDING_AREA();
  DBMS_RESOURCE_MANAGER.SUBMIT_PENDING_AREA();
end;
/
