/* =============================================================================
   Create additional (temporary) mappings
   -----------------------------------------------------------------------------
   This adds to an existing setup. Note the consumer group names must already
   exist; this uses the same names as the example setup.
   ========================================================================== */

begin
  DBMS_RESOURCE_MANAGER.CLEAR_PENDING_AREA();
  DBMS_RESOURCE_MANAGER.CREATE_PENDING_AREA();

  DBMS_RESOURCE_MANAGER.SET_CONSUMER_GROUP_MAPPING(         -- Fake Application Member
    ATTRIBUTE      => DBMS_RESOURCE_MANAGER.ORACLE_USER,
    VALUE          => 'APPMOB%',
    CONSUMER_GROUP => 'ERM_APPLICATION_GROUP'
  );
  DBMS_RESOURCE_MANAGER.SET_CONSUMER_GROUP_MAPPING(         -- Fake BatchGroup Member
    ATTRIBUTE      => DBMS_RESOURCE_MANAGER.ORACLE_USER,
    VALUE          => 'BATCHTEST',
    CONSUMER_GROUP => 'ERM_BATCH_GROUP'
  );
  DBMS_RESOURCE_MANAGER.SET_CONSUMER_GROUP_MAPPING(         -- Fake External Member (in case we cannot access the existing ones)
    ATTRIBUTE      => DBMS_RESOURCE_MANAGER.ORACLE_USER,
    VALUE          => 'EXTTEST%',
    CONSUMER_GROUP => 'ERM_EXTERNALS_GROUP'
  );

  DBMS_RESOURCE_MANAGER.VALIDATE_PENDING_AREA();
  DBMS_RESOURCE_MANAGER.SUBMIT_PENDING_AREA();
end;
/
