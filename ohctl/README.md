## Oracle Home Management with Golden Images
This work is heavily based on a blog series by [Ludovico
Caldara](http://www.ludovicocaldara.net/dba/about/), which can be found
[here](http://www.ludovicocaldara.net/dba/oh-mgmt-7/). Ludovico is an [Oracle ACE
Director](https://www.oracle.com/technetwork/community/oracle-ace/index.html)
currently working at CERN.

**ALL CREDITS ON THIS GO TO LUDOVICO!** To me (Izzy) just the SQLite part and
some minor improvements – plus adjustments for Oracle 18c and up.

His `ohctl` (Oracle Home ConTroL) script manages Oracle homes based on „Golden
Images” (also called „Gold Images” in some places) – which basically means:

* You have one Oracle Home where no database is running
* On this home, you can apply patches without the need of any downtime
* You convert the patched home into a „Golden Image” which you then can deploy
  with minimal downtime, and erasing the need for multiple patches (patch once,
  deploy many)

Many of the steps required are automated by this script:

* Cloning an existing Oracle Home to create an (initial) Golden Image
* Managing existing Golden Images for deployment
* Deploying Golden Images to a new environment (including your „Management
  Environment” you will use for patching)
* Basic adjustment of the new environment (deploying centrally stored
  `tnsnames.ora`, `sqlnet.ora` etc)

Patches still need to be performed manually, but the deployment process is almost
fully automated. In addition to an existing Oracle Environment (what else would
you need this for), **Ludovico's script** needs:

* An NFS Export (or other network drive available on all your Oracle servers)
  to store the Golden Images on
* The Oracle Instant Client (for initial deployment on a server that does not
  yet have Oracle available); can be downloaded from
  https://www.oracle.com/database/technologies/instant-client/downloads.html
  You will need the `instaclient-basic[lite]` and `instaclient-sqlplus`
* A centrally available Oracle Database to store its own repository table in.

The latter sometimes is a no-go with isolated environments (where one doesn't
want cross-environment access). For those I've adjusted the script. So **the
version you find here** replaces the second requirement by „SQLite3 executable must
be available in the `$PATH`” – and uses an SQLite database in a location you can
configure inside the script (replacing the third requirement).

## `ohctl`
First a disclaimer: No guarantees what-so-ever. Please check the script yourself
thoroughly, you might need adjustments. Test things first before using it in
production. Make backups. And all that stuff.

A short documentation can be found in [`doc/ohctl.md`](doc/ohctl.md).

### Syntax
```
$ ./ohctl -h

 Purpose : Management of Golden Images (Oracle Homes)

 Usage   : To list the available images:
             ./ohctl -l
           To install an image on the localhost:
             ./ohctl -i <goldenimage> [-n <newname>] [-r] [-R <responseFile>]
           To create an image based on the current OH:
             ./ohctl -c [-n <newname>] [ -f ]
           To remove a golden image from the repository:
             ./ohctl -d <goldenimage> [ -f ]

 Options : -l                    List the available Oracle Homes in the golden image repository
           -i <goldenimage>      Installs locally the specified golden image. (If already deployed, an error is thrown)
                                 if the option -l is given, the list action has the priority over the deploy.
           -n <newname>          Specify a new name for the Oracle Home: use it in case you need to patch
                                 and create a new Golden Image from it or if you want to change the Golden Image name
                                 for the current Oracle Home you are converting to Image.
                                 When creating a new Image (-c), it takes the basename of the OH by default, and not the
                                 OHname inside the inventory.
           -c                    Creates a new Golden Image from the current Oracle Home.
           -d <goldenimage>      Removes the golden image from the repository
           -f                    If the Golden Image to be created exists, force the overwrite.
           -r                    Link with RAC option (install only)
           -R <responseFile>     ommit for 12c and below; required for 18c+ "cloning process"
                                 (ideally use the responseFile from DB creation)
           -h | -?               Show this help

 Example : ./ohctl -i DB12_1_0_2_BP170718_home1 -n DB12_1_0_2_BP171018_home1
                                 installs the Oracle Home DB12_1_0_2_BP170718_home1 with new name DB12_1_0_2_BP171018_home1
                                 in order to apply the Bundle Patch 171018 on it

           ./ohctl -i DB12_1_0_2_BP170718_home1
                                 installs the Oracle Home DB12_1_0_2_BP170718_home1 for normal usage

           ./ohctl -c -n DB12_1_0_2_BP180116
                                 Creates a new Golden Image named DB12_1_0_2_BP180116 from the current ORACLE_HOME

           ./ohctl -c -f
                                 Creates a new Golden Image with the name of the current OH basename, overwriting
                                 the eventual current image.
                                 E.g. if the current OH is /ccv/app/oracle/product/DB12_1_0_2_BP180116_RAC, the new GI name
                                  will be "DB12_1_0_2_BP180116_RAC"
```
