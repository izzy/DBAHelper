## Removing Oracle Homes
You'll wish to remove the old ORACLE_HOMEs once it's clear they'll no
longer be used. This can be done with the Oracle Universal Installer, from the
command line:

    runinstaller -deinstall -silent ORACLE_HOME=current_oracle_home "REMOVE_HOMES={oracle_home_to_be_removed}"

Command line options are described e.g.
[here for Oracle 10g R2](https://docs.oracle.com/cd/B19306_01/em.102/b16227/oui5_product_install.htm#i1005808).
This should de-register the specified home from the central inventory -- but not
remove any files. Check afterwards with `lsoh` to make sure, then continue the
cleanup manually by removing the entire old ORACLE_HOME dir.

**Note:** this command can also be useful if your inventory got messed up, see e.g.
[How to delete/add Oracle Home entry from central inventory](https://dbakeeda.blogspot.com/2016/03/how-to-deleteadd-oracle-home-from.html)
(using Oracle 11.2; remove a home, then re-attach it).
For a list of options and variables to be used with `runInstaller`, please see
[here](https://docs.oracle.com/cd/B28359_01/em.111/b31207/c_oui_appendix.htm#OUICG378) (again for 11g).

### Oracle 12c and up
With Oracle 12c and up, there's also a the separate [Oracle
Deinstallation Tool](https://docs.oracle.com/en/database/oracle/oracle-database/12.2/cwlin/oracle-deinstallation-tool-deinstall.html) (for Oracle 19c, [see here](https://docs.oracle.com/en/database/oracle/oracle-database/19/ladbi/oracle-deinstallation-tool-deinstall.html)).
Starting with Oracle 12.2, you'll get pointed to this when trying the above -- and at least from Oracle 19c onwards the above is no longer available at all.

    export ORACLE_HOME=<Oracle-Home-To_remove>
    $ORACLE_HOME/deinstall/deinstall [-silent]

**CAUTION:** Oracle documentation on the deinstall tool writes:

> The deinstallation tool deletes Oracle Database configuration files, user data,
> and fast recovery area (FRA) files even if they are located outside of the
> Oracle base directory path.

A safe approach to using this deinstaller and only remove the `$ORACLE_HOME` but not your databases is denying the installer the details it would need to destroy your database files. So for the `$ORACLE_HOME` you want to remove:

1. delete all contents of `$ORACLE_HOME/admin/network`
2. delete all contents of `$ORACLE_HOME/dbs` (so it cannot find FRA, data files etc)
3. use `lsoh` to show available homes
4. use `setoh` to set the environment to the home you want to remove
5. call `$ORACLE_HOME/deinstall/deinstall` (which deconfigures the `$ORACLE_HOME`, removes the files, and marks it in the inventory as `REMOVED="T"` (or removes it from there entirely, depending on Oracle version or whether it was the only one)
6. call `lsoh` again to see if you succeeded
7. use `setoh` again to set the environment to your current `$ORACLE_HOME`

In step 5, the deinstaller will ask you for the databases of the current home. The list it mentions then should be empty (i.e. `[]`). This will be your confirmation that none of your data files etc. will be touched, and just the software of the current `$ORACLE_HOME` will be „decomissioned“.

### Oracle 18c and up
Here things seem to have slightly changed, at least with Oracle 19c. Quoting from the 19c [About Oracle Deinstallation Options](https://docs.oracle.com/en/database/oracle/oracle-database/19/ladbi/about-oracle-deinstallation-options.html):

> When you run `deinstall`, if the central inventory (`oraInventory`) contains no other registered homes besides the home that you are deconfiguring and removing, then `deinstall` removes the following files and directory contents in the Oracle base directory of the Oracle Database installation owner: …

Which implies those are not removed otherwise – so steps 1 and 2 (removing contents from `$ORACLE_HOME/admin/network` and `$ORACLE_HOME/dbs`) should no longer be needed. In fact, if you do that, `deinstall` simply crashes with an `error: null`, the stack trace indicating issues accessing or evaluating some unidentified path). From the same page:

> If the software in the Oracle home is not running (for example, after an unsuccessful installation), then deinstall cannot determine the configuration,
> and you must provide all the configuration details either interactively or in a response file.

Note the response file asked for uses keywords different from the response file created at installation. To create the needed response file, you'd need to run `deinstall` with the parameters `-silent -checkonly` – which in this state again crashes with the `error: null`.