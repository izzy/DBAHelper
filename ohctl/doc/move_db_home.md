## Move Oracle DB to new home after deploying Golden Image
* shutdown the database (and listener, if it should move as well)
* copy instance files
  - `$ORACLE_HOME/dbs/*`
* adjust config files
  - `/var/opt/oracle/oratab` (Solaris) / `/etc/oratab` (other -nixes and -nuxes)
  - potentially: `listener.ora` (`(ORACLE_HOME=`) – don't forget to restart the
    listener!
  - potentially `sqlnet.ora` (`ADR_BASE`)
* set environment to point to new home (e.g. via `setoh`/`oraenv`)
  - adjust `$ORACLE_HOME` etc in `.profile`/`.bashrc`/`.bash_profile`/`inst.ksh`
    if it's set there (make sure to source them again if other environment
    variables or aliases are based on `$ORACLE_HOME`)
  - maybe additional scripts which set this explicitly?
* make sure all configuration files are in place in the new ORACLE_HOME:
  - `listener.ora`, `sqlnet.ora`, `tnsnames.ora`  
    `ohctl` can take care for that if they are located at a central place
    (it does symlink them from a global `$TNS_ADMIN` to `$ORACLE_HOME/network/admin`)
  - if used: ONAMES, LDAP
* if used: Oracle Clusterware:  
  `srvctl modify database –d $SID -o $ORAHOME`
* test database startup  
  not required, but probably a good idea: do this step-by-step checking for
  potential problems:
  - `startup nomount`, `alter database mount` (do not open yet)
  - shutdown again (`shutdown immediate`)
* if the new ORACLE_HOME has a higher patch level than the one you're coming
  from, make sure to apply the `datapatch` of those patch sets before you open
  the database to applications:
  - startup in restricted mode: `startup upgrade`
  - back in the shell, perform your `datapatch -verbose`
  - shutdown the database again: `shutdown immediate`
* if all went fine, make the database available again:
  - in the shell: `lsnrctl start` (if it was shut down in first step)
  - in `sqlplus`: `startup` and optionally `alter system register`

(based on [this blog](https://malcolmchalmers.wordpress.com/2009/06/03/moving-a-database-to-a-new-oracle_home/))

If Oracle Enterprise Manager is used, updates might be needed there as well; see e.g.
[Use EMCLI to update OEM Oracle Home Path](https://dba.stackexchange.com/questions/93595/30273)
for how to update the `$ORACLE_HOME` of a database there (details on the `modify_target` verb
can be found [in the OEM documentation](https://docs.oracle.com/cd/E24628_01/em.121/e17786/cli_verb_ref.htm#EMCLI1203)).


### If additional to patching, more file locations have been adjusted (unlikely)
If more path changes took place, some extra steps might be required (see e.g.
[here](http://logic.edchen.org/how-to-move-oracle-database/)):

* export the parameter file **BEFORE* the initial shutdown:  
  `SQL> create pfile from spfile;`
* adjust parameters: `*.audit_file_dest`, `*.control_files`, `*.db_recovery_file_dest`, `*.diagnostic_dest`
* re-create SPFILE **BEFORE** startup (while „connected to an idle instance”):
  `SQL> create spfile from pfile;`

If you also moved the data files and control files (why would you do that?), see
above link for details on remapping.


### More?
* RAC, Clusterware, OWB: [see here](https://jhdba.wordpress.com/2013/03/20/migrating-an-oracle-home-dead-easy/)
* ???
