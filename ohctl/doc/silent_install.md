## Silent install
### Preamble
Before you can create your first Golden Image, you certainly need an initial database installation. As often, the server dedicated to that is running „headless“ (i.e. you will not have any X environment and thus cannot run the graphical installer), this document has a short summary on how to perform „silent installs“ – with or without response files.

When planning your first install and the directory structures, please note the following advice from [About Oracle Deinstallation Options](https://docs.oracle.com/en/database/oracle/oracle-database/19/ladbi/about-oracle-deinstallation-options.html):

> Oracle strongly recommends that you configure your installations using an Optimal Flexible Architecture (OFA) configuration, and that you reserve Oracle
> base and Oracle home paths for exclusive use of Oracle software. If you have any user data in these locations in the Oracle base that is owned by the user
> account that owns the Oracle software, then deinstall deletes this data.  
> **Caution:** `deinstall` deletes Oracle Database configuration files, user data, and fast recovery area (FRA) files even if they are located outside of the Oracle base directory path.

Which in short means you start at `/u01`, with an `$ORACLE_BASE` of `/u01/app/oracle` for your databases, and have the software below `/u01/app/oracle/product/`. This is also what `ohctl` documentation assumes, with the `ORACLE_HOMES` being in `/u01/app/oracle/product/DB<version>[_patch]`, eg. `/u01/app/oracle/product/DB12_2_0_1_RU190714`.

Below hints for silent installations are taken from MOS [Doc ID 885643.1](https://support.oracle.com/CSP/main/article?cmd=show&type=NOT&id=885643.1). I've skipped the hints on 12.1 and older (as they are no longer supported at the time of this writing); please follow the link and have your MOS credentials ready if you need those.

Adjust the values in below snippets to reflect your needs (e.g. replace `EE` by `SE` if you only have licensed a Standard Edition, set the correct paths and groups) before running the statements.


### 12.2 without response file
```
./runInstaller -silent -debug -force \
FROM_LOCATION=/stage/12.2.0.1.0/database/stage/products.xml \
oracle.install.option=INSTALL_DB_SWONLY \
UNIX_GROUP_NAME=oinstall \
ORACLE_HOME=/u01/app/oracle/product/12.2.0.1 \
ORACLE_BASE=/u01/app/oracle \
oracle.install.db.InstallEdition=EE \
oracle.install.db.DBA_GROUP=dba \
oracle.install.db.OPER_GROUP=dba \
oracle.install.db.OSBACKUPDBA_GROUP=dba \
oracle.install.db.OSDGDBA_GROUP=dba \
oracle.install.db.OSKMDBA_GROUP=dba \
oracle.install.db.OSRACDBA_GROUP=dba \
DECLINE_SECURITY_UPDATES=true
```

### 18c/19c with response file
**Important note:** unpack the install archive directly into the target `$ORACLE_HOME`. Starting with 18c, there's no more „copying the installation files“ by the installer. It basically just performs the „inventory actions“ and creation of the „root scripts“ – response file or not, silent or GUI.

```
$ORACLE_HOME/runInstaller -silent -responseFile <responsefilename>
```

### 18/19c without response file

```
./runInstaller -silent -debug -force \
oracle.install.option=INSTALL_DB_SWONLY \
UNIX_GROUP_NAME=oinstall \
ORACLE_HOME=/refresh/app/oracle/product/19.3 \
ORACLE_BASE=/refresh/app/oracle \
oracle.install.db.InstallEdition=EE \
oracle.install.db.DBA_GROUP=dba \
oracle.install.db.OPER_GROUP=dba \
oracle.install.db.OSBACKUPDBA_GROUP=dba \
oracle.install.db.OSDGDBA_GROUP=dba \
oracle.install.db.OSKMDBA_GROUP=dba \
oracle.install.db.OSRACDBA_GROUP=dba \
DECLINE_SECURITY_UPDATES=true
```
