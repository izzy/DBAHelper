## `ohctl`
First a repetition of the disclaimer: No guarantees what-so-ever. Please check the script yourself thoroughly, you might need adjustments. Test things first before using it in production. Make backups. And all that stuff.

The name stands for <b>O</b>racle <b>H</b>ome <b>C</b>on<b>t</b>ro<b>l</b>. These scripts will help you with the maintenance of your `$ORACLE_HOME`s on Unix-like operating systems (Linux, Solaris etc). I have not tested it on any Windows installation; as the scripts use Bash, it would at least need Cygwin there – or the WSL.

### Requirements
Apart from the Oracle Software itself which it is intended to maintain, you will need:

* **Storage:** a place to store `ohctl` itself, plus its Golden Images. For the amount, see the size of your current Oracle installation (`$ORACLE_HOME`) and multiply it with 4. More is better, especially if you want to keep a „history“. If you have Oracle installations on multiple machines with the same architecture and Oracle versions, this storage ideally is located on a network drive (e.g. NFS) accessible by all of them.
* **SQLite:** SQLite binaries, ideally version 3 (`sqlite3`).
* **Bash:** version 4 or above.

That's it.

### Scripts and their purpose
* `ohctl`: the main script with the Golden Image management functionality
* `ohenv`: helpers used by `ohctl` – but also useful without. If you „source“ this into your environment, 3 functions become available at your shell prompt:
    * `lsoh`: show all available `$ORACLE_HOME`s
    * `setoh`: switch to a different `$ORACLE_HOME`
    * `ohpath`: include the `ohctl` directory with your `$PATH`

### Configure `ohctl`
Configuration can be done at the top of the script (in the original script, this
was in the middle of the script – after the function definitions and before the
main logic started). Each setting is accompanied by a short description, so
please see there for details. `ohctl` also relies on `ohenv`, where you might
need to adjust `ORAINSTLOC`.

### Preparing the SQLite database
Just run the Bash script `create_sqlite_db` and then copy the (empty) initial
database file `ohctl.db` to the location you configured in the previous chapter.

### Preparing the environment for `ohctl`
`ohctl` calls to one Bash function called `setoh` (to set up the environment for
the database to work on). You find this function in `ohenv`. You might wish
to integrate this (and the `lsoh()` function contained in the same script) with
your environment, e.g. by copying it into your `.bashrc`. If you do so, do NOT
comment out the call that sources `ohenv` from `ohctl` as it otherwise does not
find the function.

Second thing you need to have ready is the directory holding your Golden Images.
You might wish to store the script(s) in the same place, to have them available
in all environments you want to use them; this location should be on a network
drive (e.g. a NFS export).

### Running `ohctl`
To get available parameters and options, simply start the script with the
parameter `-h` („help”) or `-?`.


## Working with these scripts
### List available homes
The function `lsoh()` can be used for this. Simply call it without any parameters
and check the output. You should see something like

    oracle $> lsoh
    HOME                        LOCATION                            VERSION      EDITION
    --------------------------- ----------------------------------- ------------ -------
    OraGI12Home1                /u01/app/grid/product/grid          12.1.0.2.0   GRID
    OraDB12Home1                /u01/app/oracle/product/12.1.0.2    12.1.0.2.0   DBMS EE

### Creating your first Golden Image
When you start, you won't have a single Golden Image available. So the first
thing you certainly want to do is to create one, cloning it from an existing
Oracle Home. While doing so, you might wish to take care for naming conventions
(read the blog articles for why and other details on this).

    ./ohctl -c -n <new_name>

will create your Golden Image from the current `ORACLE_HOME` and give it the
name you passed with `-n`. Assuming you have a „fresh install” of Oracle 12.1.0.2
at `/u01/app/oracle/product/12.1.0.2` (as in the above example), and followed the
blog and its reasoning, your actual command line could look like this:

    ./ohctl -c -n DB12_1_0_2

Follow the progress to see if everything worked out as it should: the new Golden
Image should have been created and recorded with `ohctl`'s repository. If at any
time you need to start over with the same Golden Image, add the `-f` parameter
to the command to tell `ohctl` you want to replace the existing copy (else it
will refuse to do so, assuming you made a mistake):

    ./ohctl -c -n DB12_1_0_2 -f

### List available Golden Images
Now you've created one, it should be recorded to the repository. Let's check:

    oracle $> ./ohctl -l
    
    Listing existing golden images:
    
    OH_Name                             Created    Installed locally?
    ----------------------------------- ---------- ------------------
    12_1_0_2                            2018-02-06 Not installed

No, it's not installed – but that's correct: you didn't install the Golden Image
to an `ORACLE_HOME` but rather walked the opposite direction: created a Golden
Image *from* an existing `ORACLE_HOME`. So all is fine (though the real reason
for showing "Not installed" is that the names don't match: `ohctl` compares the `ORACLE_HOME` names from its database with those from Oracle's inventory).

### Deploying (installing) a Golden Image
Now you want to work with your first Golden Image. You want deploy it to a
location where no database runs in (maybe even on a different machine) to bring
it up to snuff – i.e. apply the latest patch. While when you're reading this
that will certainly be a different one, let's stick to Ludovico's examples and
assume the July 2017 patch:

    ./ohctl -i DB12_1_0_2 -n DB12_1_0_2_BP170718

This installs the `DB12_1_0_2` image while naming the new home differently, e.g.
`DB12_1_0_2_BP170718` – so the name reflects the patch set as well. Follow the
output and read it carefully. It will tell you there's an additional step you
need to perform manually: running the `root.sh` to complete the setup of the new
home. Check with `lsoh` and see it is available now:

    oracle $> lsoh
    HOME                  LOCATION                            VERSION      EDITION
    --------------------- ----------------------------------- ------------ -------
    OraGI12Home1          /u01/app/grid/product/grid          12.1.0.2.0   GRID
    OraDB12Home1          /u01/app/oracle/product/12.1.0.2    12.1.0.2.0   DBMS EE
    DB12_1_0_2_BP170718   /u01/app/oracle/product/DB12_1_0_2_BP170718  12.1.0.2.0   DBMS EE

**Note:** when deploying a Golden Image to a machine that does not yet have
any Oracle installation, OUI might forget to create the global `oraInst.loc` file
(Linux: `/etc/oraInst.loc`; Solaris: `/var/opt/oracle/oraInst.loc`); you will
find that mentioned at the end of `ohctl`'s output, like

    grep: /etc/oraInst.loc: No such file or directory
    grep: /ContentsXML/inventory.xml: No such file or directory
    The image DB12_1_0_2_RU190716 has been installed but it is not in the inventory. Please check.

To fix this, copy the file manually from the machine the Golden Image was created
on, and adjust it correspondingly (permissions and ownership).

**Oracle 18+:** Oracle changed how the cloning process works. You now need to
supply a response-file (e.g. the one created when you installed the database
software for the first time). This is reflected at the command-line by adding
the `-R` parameter, e.g.:

    ./ohctl -i DB19_9_0_0_RU201020 -R /path/to/responseFile.rsp

If you do not specify `-R` with the response-file, `ohctl` will stick to the
cloning mode used before. This will work fine up to 12.2, but definitely fail
with 19c.


### Patching
Now is patching time. And no need to ask a downtime for that: there's no
database running in this environment. So apply the patchset (in this example:  `170718`) to the new `ORACLE_HOME`. When done successfully, you want to create a Golden Image of this patched home. The new naming conventions are already met, so we can go easy:

    oracle $> ./ohctl -c
    oracle $> ./ohctl -l
    
    Listing existing golden images:
    
    OH_Name                             Created    Installed locally?
    ----------------------------------- ---------- ------------------
    DB12_1_0_2_BP170718                 2018-02-07 Installed
    12_1_0_2                            2018-02-06 Not installed

Now you can deploy the patched `ORACLE_HOME` to your real environments. The
only downtime you will need is for switching your databases to the new home –
which is significantly smaller than the patching time alone. Please refer to
[`doc/move_db_home.md`](move_db_home.md) for things you might need to do in this phase.

### Removing Golden Images
Over the years, you'd accumulate quite a lot of Golden Images this way. And you
will certainly want to reclaim space of those you no longer need. `ohctl`
of course also provides means for this:

    oracle $> ./ohctl -d 12_1_0_2

### Removing Oracle Homes
Similarly, you'll wish to remove the old ORACLE_HOMEs once it's clear they'll no
longer be used. Details on this you can find in the file `doc/remove_oracle_home.md`.
